import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

const DEFAULT_TOAST_DURATION = Toast.LENGTH_SHORT;
const DEFAULT_TOAST_COLOR = Color(0xFF424242);

class ToastUtils {
  ToastUtils._internal();

  ///全局初始化Toast配置, child为MaterialApp

  static void toast(String msg,
      {Toast duration = DEFAULT_TOAST_DURATION,
        Color color = DEFAULT_TOAST_COLOR}) {
    Fluttertoast.showToast(msg: msg, backgroundColor: color, toastLength: duration, gravity: ToastGravity.BOTTOM);
  }

  static void waring(String msg, {Toast duration = DEFAULT_TOAST_DURATION}) {
    Fluttertoast.showToast(msg: msg, toastLength: duration, backgroundColor: Colors.yellow);
  }

  static void error(String msg, {Toast duration = DEFAULT_TOAST_DURATION,ToastGravity position = ToastGravity.BOTTOM}) {
    Fluttertoast.showToast(msg: msg, toastLength: duration, backgroundColor: Colors.red,gravity: position);
  }

  static void success(String msg,
      {Toast duration = DEFAULT_TOAST_DURATION}) {
    Fluttertoast.showToast(msg:msg, toastLength: duration, backgroundColor: Colors.lightGreen);
  }
}
