
import 'package:shared_preferences/shared_preferences.dart';

class SPUtils {
  /// 内部构造方法，可避免外部暴露构造函数，进行实例化
  SPUtils._internal();

  static SharedPreferences? _spf ;

  static Future<SharedPreferences> init() async {
    _spf ??= await SharedPreferences.getInstance();
    return _spf!;
  }

  ///IP
  static Future<bool> saveIP(String ip) {
    return _spf!.setString('ip', ip);
  }

  ///密码
  static Future<bool> savePwd(String pwd) {
    return _spf!.setString('password', pwd);
  }

  ///学号
  static Future<bool> saveId(String id) {
    return _spf!.setString('id', id);
  }

  static String? getId(){
    return _spf!.getString('id');
  }

  static String? getIP(){
    return _spf!.getString('ip');
  }



  static String? getPwd(){
    return _spf!.getString('password');
  }


  ///是否同意隐私协议
  static Future<bool> saveIsAgreePrivacy(bool isAgree) {
    return _spf!.setBool('key_agree_privacy', isAgree);
  }

  static bool? isAgreePrivacy() {
    if (!_spf!.containsKey('key_agree_privacy')) {
      return false;
    }
    return _spf!.getBool('key_agree_privacy');
  }
}
