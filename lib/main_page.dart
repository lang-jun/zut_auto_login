import 'package:dart_ping/dart_ping.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:login_by_library/responsive.dart';
import 'package:login_by_library/utils/SPUtils.dart';
import 'package:login_by_library/utils/toastUtils.dart';
import 'package:wifi_iot/wifi_iot.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final _idController = TextEditingController();
  final _pwdController = TextEditingController();

  //运营商
  var _comp;

  late var comText;

  late var internalIP;

  bool _isLogin = false;

  var _isShowSelectComp = false;

  Future<int> pingTest() async {
    final ping = await Ping('baidu.com', count: 5).stream.first;
    if (ping.error == null) {
      return 200;
    } else {
      return 500;
    }
  }

  Future<bool> isLogin() async {
    RegExp regExp = RegExp(r'10\.13\d\.\d*?\.\d+');
    internalIP = await WiFiForIoTPlugin.getIP();
    if (internalIP == null || !regExp.hasMatch(internalIP)) return false;
    //尝试ping校园内网地址，如果能ping通，则说明已经登录。
    var pingData = await Ping(internalIP, count: 5).stream.first;
    var WLANCode = await pingTest();
    if (pingData.error == null && WLANCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  logout() async {
    String logoutUrl =
        'http://1.1.1.1:801/eportal/?c=ACSetting&a=Logout&wlanuserip=null&wlanacip=null&wlanacname=null&port=&hostname=1.1.1.1&iTermType=1&session=null&queryACIP=0&mac=a864f1daa066';
    var head = {
      "Accept":
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
      "Accept-Encoding": 'gzip, deflate',
      "Accept-Language": "zh-CN,zh;q=0.9",
      "Cache-Control": "max-age=0",
      "Connection": "keep-alive",
      "Content-Length": 0,
      "Content-Type": "application/x-www-form-urlencoded",
      "Cookie": "program=NEW; vlan=1234; ip=${SPUtils.getIP()}",
      "Host": "1.1.1.1:801",
      "Origin": "http://1.1.1.1",
      "Referer": "http://1.1.1.1/",
      "Upgrade-Insecure-Requests": 1,
      "User-Agent":
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.5735.289 Safari/537.36"
    };

    Dio dio = Dio();
    dio.options.followRedirects = true;
    dio.options.maxRedirects = 5;
    try {
      var response = await dio.post(logoutUrl, options: Options(headers: head));
      print(response.toString());
    } catch (e) {
      print('Error: $e');
      var code = await pingTest();
      if (code == 500) {
        //注销成功
        setState(() {
          _isLogin = false;
        });
        ToastUtils.toast('注销成功');
      } else {
        ToastUtils.error('注销失败');
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _idController.text = SPUtils.getId() ?? "";
    _pwdController.text = SPUtils.getPwd() ?? "";
    comText = '请选择运营商';
    isLogin().then((value) =>
        setState(() {
          _isLogin = value;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          Positioned(
            top: 100,
            left: Responsive.isTablet(context)
                ? (MediaQuery
                .of(context)
                .size
                .width - 518) / 2
                : (MediaQuery
                .of(context)
                .size
                .width - 308) / 2,
            right: 0,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: Image.asset('assets/images/logo.png',
                      width: Responsive.isTablet(context) ? 100 : 60,
                      height: Responsive.isTablet(context) ? 100 : 60),
                ),
                Image.asset(
                  'assets/images/img.png',
                  width: Responsive.isTablet(context) ? 400 : 230,
                  fit: BoxFit.fitWidth,
                ),
              ],
            ),
          ),
          Positioned(
            top: Responsive.isTablet(context) ? 230 : 180,
            left: 0,
            right: 0,
            child: _isLogin
                ? Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                const Text('已使用时间 Used time: No Record'),
                const Text('已使用流量 Used flux: 0.0MByte'),
                const Text('余额 Balance: RMB0'),
                const SizedBox(height: 20),
                GestureDetector(
                  onTap: () async {
                    //请用户确认是否要注销
                    showCupertinoDialog(
                        context: context,
                        builder: (c) {
                          return CupertinoAlertDialog(
                              title: const Text("注销"),
                              content: const Text("确定要注销吗？"),
                              actions: [
                                CupertinoDialogAction(
                                    child: const Text("取消"),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                                CupertinoDialogAction(
                                    child: const Text("确定"),
                                    onPressed: () async {
                                      Navigator.pop(context);
                                      logout();
                                    })
                              ]);
                        });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xffb84132),
                      borderRadius: BorderRadius.circular(3),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        ),
                      ],
                    ),
                    width: 150,
                    height: 25,
                    child: const Center(
                        child: Text(
                          '注销',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )),
                  ),
                )
              ],
            )
                : Column(
              children: [
                Container(
                    width: Responsive.isTablet(context) ? 400 : 200,
                    decoration:
                    const BoxDecoration(color: Colors.transparent),
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 0,
                                blurRadius: 7,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(3),
                            ),
                            shadowColor: Colors.black,
                            surfaceTintColor: Colors.transparent,
                            color: const Color(0xfff6f6f6),
                            borderOnForeground: false,
                            elevation: 3,
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                  const EdgeInsets.only(top: 8.0),
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0,
                                            right: 0,
                                            top: 8.0,
                                            bottom: 8.0),
                                        child: Image.asset(
                                            'assets/images/id.png',
                                            width: 15,
                                            height: 15),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: Text(
                                          '身 份 认 证 ',
                                          style: TextStyle(fontSize: 10),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                    padding: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            border: Border.all(
                                              color:
                                              const Color(0xffd8d8d8),
                                              width: 1,
                                            ),
                                          ),
                                          height: 30,
                                          child: TextField(
                                            cursorWidth: 1,
                                            textAlignVertical:
                                            TextAlignVertical.top,
                                            cursorColor: Colors.black,
                                            controller: _idController,
                                            // focusNode: _passwordFocusNode,
                                            // obscureText: !_isShowPassWord,
                                            decoration: InputDecoration(
                                                prefixIconConstraints:
                                                const BoxConstraints(
                                                  minWidth: 30,
                                                  minHeight: 30,
                                                ),
                                                prefixIcon: Padding(
                                                  padding:
                                                  const EdgeInsets
                                                      .all(0),
                                                  child: Container(
                                                      width: 25,
                                                      color: const Color(
                                                          0xfff1f1f1),
                                                      child: const Icon(
                                                        Icons.person,
                                                        size: 20,
                                                        color: Colors
                                                            .black38,
                                                      )),
                                                ),
                                                border: InputBorder.none,
                                                hintText: "学号",
                                                //字体居中
                                                fillColor: Colors.white,
                                                hintStyle:
                                                const TextStyle(
                                                    fontSize: 12,
                                                    color: Colors
                                                        .black38),
                                                filled: true,
                                                contentPadding:
                                                const EdgeInsets.only(
                                                    top: -5,
                                                    left: 50)),
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                Container(
                                    padding: const EdgeInsets.only(
                                        left: 10, right: 10, top: 5),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(3),
                                            color: Colors.transparent,
                                            border: Border.all(
                                              color:
                                              const Color(0xffd8d8d8),
                                              width: 1,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.5),
                                                spreadRadius: -15,
                                                blurRadius: 7,
                                                offset:
                                                const Offset(0, 3),
                                              ),
                                            ],
                                          ),
                                          height: 30,
                                          child: TextField(
                                            cursorWidth: 1,
                                            cursorColor: Colors.black,
                                            controller: _pwdController,
                                            decoration: InputDecoration(
                                                prefixIconConstraints:
                                                const BoxConstraints(
                                                  minWidth: 30,
                                                  minHeight: 30,
                                                ),
                                                prefixIcon: Padding(
                                                  padding:
                                                  const EdgeInsets
                                                      .all(0),
                                                  child: Container(
                                                      width: 25,
                                                      color: const Color(
                                                          0xfff1f1f1),
                                                      child: const Icon(
                                                        Icons
                                                            .lock_rounded,
                                                        size: 20,
                                                        color: Colors
                                                            .black38,
                                                      )),
                                                ),
                                                border: InputBorder.none,
                                                hintText: "密码",
                                                fillColor: Colors.white,
                                                hintStyle:
                                                const TextStyle(
                                                    fontSize: 12,
                                                    color: Colors
                                                        .black38),
                                                filled: true,
                                                contentPadding:
                                                const EdgeInsets.only(
                                                    top: -5)),
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              _isShowSelectComp = true;
                                            });
                                          },
                                          child: Container(
                                              margin:
                                              const EdgeInsets.only(
                                                  top: 10,
                                                  left: 15,
                                                  right: 15),
                                              width: double.infinity,
                                              height: 30,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: const Color(
                                                      0xffb6b6b6),
                                                  width: 1,
                                                ),
                                              ),
                                              child: Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets
                                                        .symmetric(
                                                        horizontal:
                                                        8.0),
                                                    child: Text(
                                                      comText,
                                                      style:
                                                      const TextStyle(
                                                          fontSize:
                                                          10),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  const Padding(
                                                      padding:
                                                      EdgeInsets.only(
                                                          left: 5.0,
                                                          right: 0,
                                                          top: 8.0,
                                                          bottom:
                                                          8.0),
                                                      child: Icon(
                                                        Icons
                                                            .keyboard_arrow_down_outlined,
                                                        size: 15,
                                                        color: Colors
                                                            .black38,
                                                      )),
                                                ],
                                              )),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          margin: const EdgeInsets.only(
                                              top: 10),
                                          height: 25,
                                          child: ButtonTheme(
                                            child: TextButton(
                                              style: ButtonStyle(
                                                padding:
                                                MaterialStateProperty
                                                    .all(
                                                    const EdgeInsets
                                                        .all(0)),
                                                backgroundColor:
                                                MaterialStateProperty
                                                    .all(const Color(
                                                    0xffba4232)),
                                                minimumSize:
                                                MaterialStateProperty
                                                    .all(const Size(
                                                    140, 0)),
                                                shape: MaterialStateProperty
                                                    .all(
                                                    RoundedRectangleBorder(
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(3),
                                                    )),
                                              ),
                                              child: const Text(
                                                '登 录',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                              ),
                                              onPressed: () async {
                                                //执行登录操作
                                                //输出学号和密码和运营商
                                                var id =
                                                    _idController.text;
                                                var pwd =
                                                    _pwdController.text;

                                                if (id == "" ||
                                                    pwd == "") {
                                                  ToastUtils.waring(
                                                      "学号或密码不能为空");
                                                  return;
                                                }
                                                if (_comp == null) {
                                                  ToastUtils.waring(
                                                      "请选择运营商");
                                                  return;
                                                }
                                                Map<String, Object> obj =
                                                await onSubmit(
                                                    id, pwd, _comp);
                                                if (obj['code'] == 1) {
                                                  ToastUtils.success(
                                                      obj['msg']
                                                          .toString());
                                                  setState(() {
                                                    _isLogin = true;
                                                  });
                                                  //保存学号和密码
                                                  SPUtils.saveId(id);
                                                  SPUtils.savePwd(pwd);
                                                } else if (obj['code'] ==
                                                    2) {
                                                  ToastUtils.error(
                                                      obj['msg']
                                                          .toString());
                                                }
                                              },
                                            ),
                                          ),
                                        ),
                                        TextButton(
                                            child: const Text('免责声明',
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black)),
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return CupertinoAlertDialog(
                                                        title: const Text(
                                                            '免责声明'),
                                                        content: const Text(
                                                            '本APP不会收集您的任何个人信息，学号和密码保存在了本地，我们并不能获取。另外，本APP仅供学习交流，不得利用本APP做任何有损于学校利益的事情。否则，后果由您个人承担。'),
                                                        actions: [
                                                          CupertinoDialogAction(
                                                              child:
                                                              const Text(
                                                                  "确定"),
                                                              onPressed:
                                                                  () {
                                                                Navigator.pop(
                                                                    context);
                                                              }),
                                                        ]);
                                                  });
                                            }),
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        Transform.scale(
                          scale: Responsive.isTablet(context) ? 2 : 1,
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: Responsive.isTablet(context)
                                    ? 28.0
                                    : 8.0),
                            child: const Text(
                                '使用方法:\n1.连接上zut-stu(不需要登录)\n2.正常填写表单登录\n3.当你的校园网被其他设备占用，需要登录两次',
                                style: TextStyle(
                                    fontSize: 10, color: Colors.black)),
                          ),
                        )
                      ],
                    )),
              ],
            ),
          ),
          const Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: Center(child: Text('本APP由@苗仁豪和@李信成共同开发'))),
          !_isShowSelectComp
              ? Container()
              : GestureDetector(
            onTap: () {
              setState(() {
                _isShowSelectComp = false;
              });
            },
            child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.black38,
                child: Stack(
                  children: [
                    Positioned(
                        bottom: 20,
                        left: 20,
                        right: 20,
                        child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 18),
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 0.1,
                                            color: Colors.black38))),
                                child: RadioListTile(
                                    controlAffinity:
                                    ListTileControlAffinity.trailing,
                                    contentPadding: EdgeInsets.zero,
                                    materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                    activeColor: const Color(0xff943637),
                                    title: const Text('中国移动'),
                                    value: 'cmcc',
                                    groupValue: _comp,
                                    onChanged: (v) =>
                                    {
                                      setState(() {
                                        ('中国移动');
                                        _comp = v;
                                        comText = '中国移动';
                                        _isShowSelectComp = false;
                                      })
                                    }),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 18),
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 0.1,
                                            color: Colors.black38))),
                                child: RadioListTile(
                                    controlAffinity:
                                    ListTileControlAffinity.trailing,
                                    contentPadding: EdgeInsets.zero,
                                    materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                    activeColor: const Color(0xff943637),
                                    title: const Text('中国电信'),
                                    value: 'telecom',
                                    groupValue: _comp,
                                    onChanged: (v) =>
                                    {
                                      setState(() {
                                        _comp = v;
                                        comText = '中国电信';
                                        _isShowSelectComp = false;
                                      })
                                    }),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 18),
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 0.1,
                                            color: Colors.black38))),
                                child: RadioListTile(
                                    controlAffinity:
                                    ListTileControlAffinity.trailing,
                                    contentPadding: EdgeInsets.zero,
                                    materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                    activeColor: const Color(0xff943637),
                                    title: const Text('中国联通'),
                                    value: 'unicom',
                                    groupValue: _comp,
                                    onChanged: (v) =>
                                    {
                                      setState(() {
                                        _comp = v;
                                        comText = '中国联通';
                                        _isShowSelectComp = false;
                                      })
                                    }),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 18),
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 0.1,
                                            color: Colors.black38))),
                                child: RadioListTile(
                                    controlAffinity:
                                    ListTileControlAffinity.trailing,
                                    contentPadding: EdgeInsets.zero,
                                    materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                    activeColor: const Color(0xff943637),
                                    title: const Text('校园单宽'),
                                    value: 'founder',
                                    groupValue: _comp,
                                    onChanged: (v) =>
                                    {
                                      setState(() {
                                        _comp = v;
                                        comText = '校园单宽';
                                        _isShowSelectComp = false;
                                      })
                                    }),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 18),
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 0.1,
                                            color: Colors.black38))),
                                child: RadioListTile(
                                    controlAffinity:
                                    ListTileControlAffinity.trailing,
                                    contentPadding: EdgeInsets.zero,
                                    materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                    activeColor: const Color(0xff943637),
                                    title: const Text('校园内网'),
                                    value: 'free',
                                    groupValue: _comp,
                                    onChanged: (v) =>
                                    {
                                      setState(() {
                                        _comp = v;
                                        comText = '校园内网';
                                        _isShowSelectComp = false;
                                      })
                                    }),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 18),
                                child: RadioListTile(
                                    controlAffinity:
                                    ListTileControlAffinity.trailing,
                                    contentPadding: EdgeInsets.zero,
                                    materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                    activeColor: const Color(0xff943637),
                                    title: const Text('图书馆'),
                                    value: 'lib',
                                    groupValue: _comp,
                                    onChanged: (v) =>
                                    {
                                      setState(() {
                                        _comp = v;
                                        comText = '图书馆';
                                        _isShowSelectComp = false;
                                      })
                                    }),
                              ),
                            ],
                          ),
                        ))
                  ],
                )),
          ).animate().fadeIn(),
        ],
      ),
    );
  }

  Future<Map<String, Object>> onSubmit(id, pwd, com) async {
    RegExp regExp = RegExp(r'10\.13\d\.\d*?\.\d+');
    String? ip = await WiFiForIoTPlugin.getIP();

    if (ip == null || !regExp.hasMatch(ip)) {
      return {'code': 2, 'msg': '未连接校园网，请连接校园网后重试！'};
    }

//     // 获取内网ip地址

    // 获取10.133.x.x地址完毕
    String url =
        "http://1.1.1.1:801/eportal/?c=ACSetting&a=Login&protocol=http:&hostname=1.1.1.1&iTermType=1&wlanuserip=ThisIsMyIp&wlanacip=null&wlanacname=null&mac=00-00-00-00-00-00&ip=$ip&enAdvert=0&queryACIP=0&loginMethod=1";
    Map<String, String> head = {
      "Host": "1.1.1.1:801",
      "Content-Length": "167",
      "Cache-Control": "max-age=0",
      "Origin": "http://1.1.1.1",
      "DNT": "1",
      "Upgrade-Insecure-Requests": "1",
      "Content-Type": "application/x-www-form-urlencoded",
      "User-Agent":
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 Edg/106.0.1370.34",
      "Accept":
      "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "Referer": "http://1.1.1.1/",
      "Accept-Encoding": "gzip, deflate",
      "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
      "Connection": "close"
    };
    url = url.replaceAll("ThisIsMyIp", ip);

    // 得到请求头
    // 分离POST数据
    String postData =
    '''DDDDD=,0,ThisIsMyID@$com&upass=ThisIsMypwd&R1=0&R2=0&R3=0&R6=0&para=00&0MKKey=123456&buttonClicked=&redirect_url=&err_flag=&username=&password=&user=&cmd=&Login=''';
    print("postData: $com");
    postData = postData.replaceAll("ThisIsMyID", id);
    postData = postData.replaceAll("ThisIsMypwd", pwd);
    List<String> dataLines = postData.split("&");
    Map<String, String> data = {};
    for (String x in dataLines) {
      List<String> tmp = x.split("=");
      if (tmp.length == 2) {
        data[tmp[0]] = tmp[1];
      } else {
        data[tmp[0]] = "";
      }
    }
    // print("data: $data");
    // 得到格式化好的POSTData
    Dio dio = Dio();

    dio.options.followRedirects = true; // 开启重定向
    dio.options.maxRedirects = 5; // 设置最大重定向次数，避免无限重定向
    try {
      await dio.post(url, data: data, options: Options(headers: head));
    } catch (e) {
      print("Error: $e");
    }
    int resultCode = await pingTest();
    if (resultCode == 200) {
      SPUtils.saveIP(ip);
      return {'code': 1, 'msg': '登录成功'};
    } else {
      return {'code': 2, 'msg': '请检查输入参数是否正确，或是否开了代理'};
    }
  }
}
